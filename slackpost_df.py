#!/usr/bin/env python
# slackpost_df.py

def post_response(v):
    from httplib2 import Http
    h = Http()

    headers={'Content-type': 'application/json'}
    databody='{"text":"Hello, World! %s"}' % str(v)
    #urivar='https://hooks.slack.com/services/TL...B1P/BM...WTB/B3...s3E'
    import slack_user_tokens
    urivar='https://hooks.slack.com/services/%s' % slack_user_tokens.tokens

    resp, content = h.request(urivar, method="POST", body=databody)
    print ; print " ---resp", resp ; print " ---content", content
    return [resp, content]

import os
p = os.popen("df -h . | grep -v Avail ","r")
while 1:
    line = p.readline()
    if not line: break
    print ; print ' ---line', line ; fields = line.split(' ')
    print ' ---split', str(fields)
    rv = " "
    for x in fields:
        if len(x) >= 2 and len(x) <= 8 and x[-1] == 'G':
            rv += "%s " % x
    print ' ---rv', rv
    post_response(rv)



#!/usr/bin/env python
# test_df.py

# cfg: trigger check at the hour 17
hourtrigger = 17

# logic:
import time

def post_response(v):
    from httplib2 import Http
    h = Http()

    headers={'Content-type': 'application/json'}
    databody='{"text":" %s"}' % str(v)
    #urivar='https://hooks.slack.com/services/LB17P/KWTPB/WDslE9C'
    import slack_user_tokens
    urivar='https://hooks.slack.com/services/%s' % slack_user_tokens.tokens

    resp, content = h.request(urivar, method="POST", body=databody)
    #print ; print " --- ---  resp", resp
    print " --- ---  content", content
    return [resp, content]

def post_df( chkpath ):
    import os
    p = os.popen("df -h ./%s | grep -v Avail " % chkpath,"r")
    while 1:
        line = p.readline()
        if not line: break
        #print ; print ' --- line', line
        fields = line.split()
        #print ' --- split', str(fields)
        rv = " %8s " % chkpath
        rv += ' '.join(fields[1:-1])
        print ' --- rv', rv
        return rv

def loop_check():
    state = 0
    loopcnt = 0
    lasthour = 0
    while True:
        loopcnt += 1
        tm = time.localtime()
        dy, hr = tm[2], tm[3]
        if lasthour != hr:
            print " ### Hour ", hr, " loopcnt ", loopcnt
            lasthour = hr
        if state == 0 and hr == hourtrigger:
            time.sleep(10)
            print " ### ### ### Check df at hour ", hr, " loopcnt ", loopcnt
            state = 1
            rv0 = " %d-%d-%d %d:%d:%d " % (tm[0], tm[1], tm[2], tm[3], tm[4], tm[5])
            rv1 = post_df("./")
            rv2 = post_df("diskc/")
            rv3 = post_df("diskd/")
            rv = '```' + rv0 + '\r\n' + rv1 + '\r\n' + rv2 + '\r\n' + rv3 + '```'
            post_response(rv)
            print " ### ### ### Check df at hour ", hr, " done "
        elif hr != hourtrigger:
            state = 0
        time.sleep(10)

if __name__ == "__main__":
    loop_check()

